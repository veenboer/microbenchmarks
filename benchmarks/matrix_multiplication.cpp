#include <benchmark/benchmark.h>

#include <complex>
#include <new>  // For std::align_val_t
#include <random>

// Function to perform matrix multiplication for 2x2 complex matrices
void matrixMultiply(const std::complex<float>* A, const std::complex<float>* B,
                    std::complex<float>* C) {
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      std::complex<float> sum = 0.0f;
      for (int k = 0; k < 2; ++k) {
        sum += A[i * 2 + k] * B[k * 2 + j];
      }
      C[i * 2 + j] = sum;
    }
  }
}

inline void multiply_mat(const float* a, const float* b, float* c, float sign) {
  c[0] += sign * (a[0] * b[0] + a[1] * b[2]);
  c[1] += sign * (a[0] * b[1] + a[1] * b[3]);
  c[2] += sign * (a[2] * b[0] + a[3] * b[2]);
  c[3] += sign * (a[2] * b[1] + a[3] * b[3]);
}

void matrixMultiplyNaive(const std::complex<float>* a,
                         const std::complex<float>* b, std::complex<float>* c) {
  const float a_real[] = {a[0].real(), a[1].real(), a[2].real(), a[3].real()};
  const float b_real[] = {b[0].real(), b[1].real(), b[2].real(), b[3].real()};
  const float a_imag[] = {a[0].imag(), a[1].imag(), a[2].imag(), a[3].imag()};
  const float b_imag[] = {b[0].imag(), b[1].imag(), b[2].imag(), b[3].imag()};

  float c_real[4] = {0, 0, 0, 0};
  float c_imag[4] = {0, 0, 0, 0};

  multiply_mat(a_real, b_real, c_real, 1.0f);
  multiply_mat(a_imag, b_imag, c_real, -1.0f);
  multiply_mat(a_real, b_imag, c_imag, 1.0f);
  multiply_mat(a_imag, b_real, c_imag, 1.0f);

  c[0] = {c_real[0], c_imag[0]};
  c[1] = {c_real[1], c_imag[1]};
  c[2] = {c_real[2], c_imag[2]};
  c[3] = {c_real[3], c_imag[3]};
}
// Initialize matrices with random complex values
std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<float> dis(-1.0, 1.0);

inline void Initialize(std::complex<float>* a) {
  for (int i = 0; i < 4; ++i) {
    a[i] = std::complex<float>(dis(gen), dis(gen));
  }
}

class InitializeInput : public benchmark::Fixture {
 public:
  void SetUp(::benchmark::State& state) {
    A = static_cast<std::complex<float>*>(
        std::aligned_alloc(32, 4 * sizeof(std::complex<float>)));
    B = static_cast<std::complex<float>*>(
        std::aligned_alloc(32, 4 * sizeof(std::complex<float>)));
    C = static_cast<std::complex<float>*>(
        std::aligned_alloc(32, 4 * sizeof(std::complex<float>)));
    // Initialize matrices with random complex values
    Initialize(A);
    Initialize(B);
  }
  void TearDown(::benchmark::State& state) {
    // Free the allocated memory
    std::free(A);
    std::free(B);
    std::free(C);
  }

  std::complex<float>* A;
  std::complex<float>* B;
  std::complex<float>* C;
};

// Reference standard
BENCHMARK_F(InitializeInput, MatrixMultiplication)(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiply(A, B, C);
  }
}

// Using naive implementation
BENCHMARK_F(InitializeInput, MatrixMultiplicationNaive)
(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiplyNaive(A, B, C);
  }
}

BENCHMARK_MAIN();
