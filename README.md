# Microbenchmarks

A collection of microbenchmarks for C++ code. 


## Building

The project relies on CMake to build the code.
```
# clone the current project then create a build configuration by running
cmake -B build [path_to_repository]
make -C build
```
you can specify another build program by providing the option `-G`,
 for example to use Ninja `-G Ninja`.
 The commands executes will then be
 ```
cmake -B build [path_to_repository]
ninja -C build
 ```
To execute the code you can just run
```
build/microbenchmarks
```

## Add another benchmark
If you want to add another benchmark you can
add a c++ into benchmarks and then add it to the `CMakeLists.txt` file in the target sections.